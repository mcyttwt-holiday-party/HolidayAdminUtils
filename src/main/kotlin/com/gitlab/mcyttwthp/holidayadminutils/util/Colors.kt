package com.gitlab.mcyttwthp.holidayadminutils.util

import org.bukkit.ChatColor

object Colors {
    val PREFIX = "${ChatColor.GREEN}Holiday${ChatColor.GOLD}${ChatColor.BOLD}Admin${ChatColor.RESET}${ChatColor.LIGHT_PURPLE}Utils ${ChatColor.AQUA}>> ${ChatColor.RESET}"
}