package com.gitlab.mcyttwthp.holidayadminutils

import com.gitlab.mcyttwthp.holidayadminutils.commands.*
import com.gitlab.mcyttwthp.holidayadminutils.events.PlayerJoinListener
import com.gitlab.mcyttwthp.holidayadminutils.events.PlayerLeaveListener
import com.gitlab.mcyttwthp.holidayadminutils.queues.Queue
import org.bukkit.ChatColor
import org.bukkit.entity.Player
import org.bukkit.plugin.java.JavaPlugin
import org.bukkit.scheduler.BukkitTask

class Main : JavaPlugin() {
    private var timer: BukkitTask? = null

    override fun onEnable() {
        plugin = this

        this.server.pluginManager.registerEvents(PlayerJoinListener, this)
        this.server.pluginManager.registerEvents(PlayerLeaveListener, this)

        this.getCommand("playercount")?.setExecutor(PlayerCountCommand)

        this.getCommand("queue")?.setExecutor(GameQueueCommand)
        this.getCommand("queue")?.tabCompleter = GameQueueCommand

        this.getCommand("sudo")?.setExecutor(SudoCommand)
        this.getCommand("sudo")?.tabCompleter = SudoCommand

        this.getCommand("vanish")?.setExecutor(VanishCommand)

        this.getCommand("fly")?.setExecutor(FlyCommand)

        timer = this.server.scheduler.runTaskTimer(this, Runnable {
            server.onlinePlayers.forEach {
                vanished.forEach { van ->
                    it.hidePlayer(this, van)
                }
            }

            vanished.forEach {
                it.sendActionBar("${ChatColor.GREEN}You are currently invisible to other players!")
            }
        }, 0L, 60L)
    }

    override fun onDisable() {
        timer?.cancel()
        server.scheduler.cancelTasks(this)

        server.onlinePlayers.forEach {
            vanished.forEach { pl ->
                it.showPlayer(this, pl)
            }
        }

        queues.clear()
        vanished.clear()
    }

    companion object {
        lateinit var plugin: JavaPlugin

        val queues = mutableListOf<Queue>()
        val vanished = mutableListOf<Player>()
    }
}