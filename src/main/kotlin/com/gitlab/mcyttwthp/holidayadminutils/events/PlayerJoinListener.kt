package com.gitlab.mcyttwthp.holidayadminutils.events

import com.gitlab.mcyttwthp.holidayadminutils.Main
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerJoinEvent

object PlayerJoinListener : Listener {
    @EventHandler
    fun onPlayerJoin(ev: PlayerJoinEvent) {
        Main.vanished.forEach {
            ev.player.hidePlayer(Main.plugin, it)
        }
    }
}