package com.gitlab.mcyttwthp.holidayadminutils.events

import com.gitlab.mcyttwthp.holidayadminutils.Main
import com.gitlab.mcyttwthp.holidayadminutils.util.Colors
import org.bukkit.ChatColor
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerQuitEvent

object PlayerLeaveListener : Listener {
    @EventHandler
    fun onPlayerLeave(ev: PlayerQuitEvent) {
        if (Main.queues.any { it.players.contains(ev.player) }) {
            Main.queues.filter { it.players.contains(ev.player) }.forEach {
                it.players.remove(ev.player)

                it.creator.sendMessage("${Colors.PREFIX}Player ${ChatColor.RED}${ev.player.name}${ChatColor.RESET} disconnected from the server, so they have been removed from your game queue.")
            }
        }

        if (Main.queues.any { it.creator == ev.player }) {
            val potentialNewOwners = Main.plugin.server.onlinePlayers.filter { it.hasPermission("adminutils.queue.management") && it != ev.player }

            val queues = Main.queues.filter { it.creator == ev.player }

            // Hopefully this never occurs, but if it does
            // this will have to be a safeguard.
            if (potentialNewOwners.isEmpty()) {
                Main.queues.removeAll(queues)
                Main.plugin.logger.info("Deleted ${queues.size} game queues because all players with the permission to manage the queue have disconnected.")

                return
            }

            queues.forEach { queue ->
                val newOwner = potentialNewOwners.random()

                newOwner.sendMessage("${Colors.PREFIX}You have been made the new owner of the ${ChatColor.AQUA}${queue.name}${ChatColor.RESET} queue because the old owner ${ChatColor.RED}${ev.player.name}${ChatColor.RESET} has disconnected.")

                queue.creator = newOwner
            }
        }
    }
}