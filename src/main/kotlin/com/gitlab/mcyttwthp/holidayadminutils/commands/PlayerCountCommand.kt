package com.gitlab.mcyttwthp.holidayadminutils.commands

import com.gitlab.mcyttwthp.holidayadminutils.Main
import com.gitlab.mcyttwthp.holidayadminutils.util.Colors
import org.bukkit.ChatColor
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import kotlin.math.absoluteValue

object PlayerCountCommand : CommandExecutor {
    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {
        if (args.isEmpty())
            return false

        try {
            val count = args[0].toInt().absoluteValue

            Main.plugin.server.maxPlayers = count

            sender.sendMessage("${Colors.PREFIX}Player count was successfully changed to ${ChatColor.YELLOW}$count${ChatColor.RESET}!")
        } catch (e: NumberFormatException) {
            sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}An invalid number was sent : ${ChatColor.YELLOW}${args[0]}")
        } catch (e: Exception) {
            sender.sendMessage("${Colors.PREFIX}An exception occurred whilst executing command: ${ChatColor.RED}${e.message}")
            e.printStackTrace()
        }

        return true
    }
}