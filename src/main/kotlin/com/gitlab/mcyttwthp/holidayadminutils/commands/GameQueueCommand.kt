package com.gitlab.mcyttwthp.holidayadminutils.commands

import com.gitlab.mcyttwthp.holidayadminutils.Main
import com.gitlab.mcyttwthp.holidayadminutils.queues.Queue
import com.gitlab.mcyttwthp.holidayadminutils.util.Colors
import net.md_5.bungee.api.chat.ClickEvent
import net.md_5.bungee.api.chat.HoverEvent
import net.md_5.bungee.api.chat.TextComponent
import net.md_5.bungee.api.chat.hover.content.Text
import org.bukkit.ChatColor
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.command.TabCompleter
import org.bukkit.entity.Player
import kotlin.math.absoluteValue

object GameQueueCommand : CommandExecutor, TabCompleter {
    private val charPool : List<Char> = ('a'..'z') + ('A'..'Z') + ('0'..'9')

    override fun onTabComplete(
        sender: CommandSender,
        command: Command,
        alias: String,
        args: Array<out String>
    ): MutableList<String> {
        if (args.isEmpty()) return mutableListOf("")

        return when (args.size) {
            1 -> { // join / leave / list / (admin) create / delete
                val validArgs = mutableListOf("join", "leave", "list", "info")

                if (sender.hasPermission("adminutils.queue.management")) {
                    validArgs.addAll(mutableListOf("create", "delete", "settings", "announce", "transfer", "tp"))
                }

                validArgs
            }

            2 -> {
                if (listOf("join", "delete", "settings", "config", "announce", "transfer", "tp").contains(args[0].toLowerCase()) && Main.queues.isNotEmpty()) {
                    return Main.queues.filter { it.players.size < it.maxPlayers }.map { it.id }.toMutableList()
                }

                mutableListOf("")
            }

            3 -> {
                if (args[0].toLowerCase() == "settings" || args[0].toLowerCase() == "config") {
                    return mutableListOf(
                        "name",
                        "max-players",
                        "ready"
                    )
                }

                if (args[0].toLowerCase() == "transfer") {
                    return Main.plugin.server.onlinePlayers.filter { it.hasPermission("adminutils.queue.management") }.map {it.name}.toMutableList()
                }

                mutableListOf("")
            }

            4 -> {
                if (args[2].toLowerCase() == "ready") {
                    return mutableListOf("true", "false")
                }

                mutableListOf("")
            }

            else -> mutableListOf("")
        }
    }

    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {
        if (args.isEmpty()) return false

        when (args[0]) {
            "join" -> {
                if (args.size == 1) return false
                if (sender !is Player) {
                    sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}You're not a player!")
                    return true
                }

                if (!Main.queues.any { it.id == args[1] }) {
                    sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}That game queue doesn't exist!")
                    return true
                }

                if (Main.queues.any { it.players.contains(sender) }) {
                    sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}You're already in a queue! Please leave the queue you're currently in.")

                    return true
                }

                val queue = Main.queues.filter { it.id == args[1] }[0]

                if (queue.players.size >= queue.maxPlayers) {
                    sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}The queue is full!")
                    return true
                }

                if (queue.isReady) {
                    sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}The game queue has already started!")
                    return true
                }

                queue.players.add(sender)

                queue.creator.sendMessage("${Colors.PREFIX}Player ${ChatColor.GREEN}${sender.name} ${ChatColor.RESET}joined the queue ${ChatColor.YELLOW}${queue.name}${ChatColor.RESET}.")

                sender.sendMessage("${Colors.PREFIX}You've been added to the game queue for ${ChatColor.YELLOW}${queue.name}${ChatColor.RESET}!")

                return true
            }

            "leave" -> {
                if (sender !is Player) {
                    sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}You're not a player!")
                    return true
                }

                if (!Main.queues.any { it.players.contains(sender) }) {
                    sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}You're not in any queue!")

                    return true
                }

                val queue = Main.queues.first { it.players.contains(sender) }

                queue.players.remove(sender)
                if (queue.isReady) {
                    sender.teleport(Main.plugin.server.worlds.first().spawnLocation)
                }

                sender.sendMessage("${Colors.PREFIX}Successfully left the queue!")
                queue.creator.sendMessage("${Colors.PREFIX}Player ${ChatColor.GREEN}${sender.name} ${ChatColor.RESET}left the queue ${ChatColor.YELLOW}${queue.name}${ChatColor.RESET}.")

                return true
            }

            "list" -> {
                if (Main.queues.isEmpty()) {
                    sender.sendMessage("${Colors.PREFIX}There are no queues available at the moment!")
                    return true
                }

                sender.sendMessage("${Colors.PREFIX}Listing all game queues available...")

                val queues = Main.queues.sortedByDescending { it.players.size }

                queues.forEach {
                    sender.sendMessage("    ${ChatColor.GREEN}${queues.indexOf(it) + 1}. ${if (it.isReady || it.players.size >= it.maxPlayers) ChatColor.RED else ChatColor.GREEN}${it.name} ${ChatColor.GOLD}(ID : ${it.id}) [${ChatColor.BLUE}${it.players.size} ${ChatColor.GOLD}/ ${ChatColor.DARK_GREEN}${it.maxPlayers}${ChatColor.RESET}]")
                }

                return true
            }

            "info" -> {
                if (args.size == 1 && sender is Player && !Main.queues.any { it.players.contains(sender) }) {
                    sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}You're not in any queue!")

                    return true
                }

                if (args.size == 1 && sender !is Player) {
                    sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}You're not a player!")
                    return true
                }

                if (args.size == 1 && sender is Player) {
                    val queue = Main.queues.first { it.players.contains(sender) }

                    sender.sendMessage("${Colors.PREFIX}Info of the game queue for ${ChatColor.AQUA}${queue.name} ${ChatColor.RESET}:")
                    sender.sendMessage("    ${ChatColor.YELLOW}ID : ${ChatColor.GREEN}${queue.id}")
                    sender.sendMessage("    ${ChatColor.GREEN}Status : ${if (queue.isReady) "${ChatColor.GOLD}Ready to be playing" else "${ChatColor.GREEN}Open"}")
                    sender.sendMessage("    ${ChatColor.GOLD}Players : ${ChatColor.BLUE}${queue.players.size} ${ChatColor.GOLD}/ ${ChatColor.DARK_GREEN}${queue.maxPlayers}")
                    sender.sendMessage("    ${ChatColor.YELLOW}Queue Creator : ${ChatColor.GREEN}${queue.creator.name}")
                    sender.sendMessage("")
                    sender.sendMessage("    ${ChatColor.GREEN}Players List : ${ChatColor.GOLD}${queue.players.joinToString("${ChatColor.GREEN}, ") { "${ChatColor.GOLD}${it.name}" }}")

                    return true
                }

                if (!Main.queues.any { it.id == args[1] }) {
                    sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}That queue doesn't exist!")

                    return true
                }

                val queue = Main.queues.first { it.id == args[1] }

                sender.sendMessage("${Colors.PREFIX}Info of the game queue for ${ChatColor.AQUA}${queue.name} ${ChatColor.RESET}:")
                sender.sendMessage("    ${ChatColor.YELLOW}ID : ${ChatColor.GREEN}${queue.id}")
                sender.sendMessage("    ${ChatColor.GREEN}Status : ${if (queue.isReady) "${ChatColor.GOLD}Ready to be playing" else "${ChatColor.GREEN}Open"}")
                sender.sendMessage("    ${ChatColor.GOLD}Players : ${ChatColor.BLUE}${queue.players.size} ${ChatColor.GOLD}/ ${ChatColor.DARK_GREEN}${queue.maxPlayers}")
                sender.sendMessage("    ${ChatColor.YELLOW}Queue Creator : ${ChatColor.GREEN}${queue.creator.name}")
                sender.sendMessage("")
                sender.sendMessage("    ${ChatColor.GREEN}Players List : ${ChatColor.GOLD}${queue.players.joinToString("${ChatColor.GREEN}, ") { "${ChatColor.GOLD}${it.name}" }}")

                return true
            }

            // Admin
            "create" -> {
                if (!sender.hasPermission("adminutils.queue.management")) {
                    sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}You don't have permission to use this command!")
                    return true
                }

                if (sender !is Player) {
                    sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}You are not a player!")
                    return true
                }

                if (args.size <= 3) {
                    sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}Not enough arguments! Usage of this command is ${ChatColor.YELLOW}/queue create <maxPlayers> <name>")
                    return true
                }

                val maxPlayers = args[1].toInt().absoluteValue
                val name = args.slice(2 until args.size).joinToString(" ")

                sender.sendMessage("${Colors.PREFIX}Created game queue ${ChatColor.AQUA}${name} ${ChatColor.RESET}with max player count of ${ChatColor.YELLOW}${maxPlayers}${ChatColor.RESET}!")

                val id = (1..4).map {
                    kotlin.random.Random.nextInt(0, charPool.size)
                }
                    .map(charPool::get)
                    .joinToString("")

                Main.queues.add(Queue(
                    id = id,
                    name = name,
                    maxPlayers = maxPlayers,
                    players = mutableListOf(),
                    creator = sender,
                    isReady = false
                ))

                announceGame(id)

                return true
            }

            "delete" -> {
                if (!sender.hasPermission("adminutils.queue.management")) {
                    sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}You don't have permission to use this command!")
                    return true
                }

                if (args.size == 1) {
                    sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}No ID was specified!")
                    return true
                }

                if (!Main.queues.any { it.id == args[1] }) {
                    sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}That game queue doesn't exist!")
                    return true
                }

                try {
                    Main.queues.removeIf { it.id == args[1] }
                    sender.sendMessage("${Colors.PREFIX}Successfully deleted game queue.")
                } catch (e: Exception) {
                    sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}An exception occurred whilst executing the command : ${ChatColor.GOLD}${e.message}")
                    e.printStackTrace()
                }

                return true
            }

            "settings", "config" -> {
                if (!sender.hasPermission("adminutils.queue.management")) {
                    sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}You don't have permission to use this command!")
                    return true
                }

                if (args.size == 1 || args.size == 2) {
                    sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}Not enough arguments!")
                    return true
                }

                if (!Main.queues.any { it.id == args[1] }) {
                    sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}The game queue doesn't exist!")
                    return true
                }

                val queue = Main.queues.first { it.id == args[1] }

                when (args[2].toLowerCase()) {
                    "name" -> {
                        if (args.size == 3) {
                            sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}Value must be a string!")
                            return true
                        }

                        val name = args.slice(3 until args.size).joinToString(" ")

                        sender.sendMessage("${Colors.PREFIX}Successfully set game queue setting ${ChatColor.GOLD}\"name\"${ChatColor.RESET} to ${ChatColor.GREEN}\"$name\"${ChatColor.RESET}.")
                        queue.name = name

                        return true
                    }

                    "max-players" -> {
                        if (args.size == 3 || args[3].toIntOrNull() == null) {
                            sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}Value must be a number!")
                            return true
                        }

                        val maxPlayers = args[3].toInt().absoluteValue

                        sender.sendMessage("${Colors.PREFIX}Successfully set game queue setting ${ChatColor.GOLD}\"max-players\"${ChatColor.RESET} to ${ChatColor.GREEN}$maxPlayers${ChatColor.RESET}.")
                        queue.maxPlayers = maxPlayers

                        return true
                    }

                    "ready" -> {
                        if (args.size == 3) {
                            sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}Value must be true or false!")
                            return true
                        }

                        return if (args[3].toLowerCase() == "true") {
                            sender.sendMessage("${Colors.PREFIX}Successfully set game queue setting ${ChatColor.GOLD}\"ready\" ${ChatColor.RESET}to ${ChatColor.GREEN}\"true\"${ChatColor.RESET}.")
                            queue.isReady = true

                            true
                        } else if (args[2].toLowerCase() == "false") {
                            sender.sendMessage("${Colors.PREFIX}Successfully set game queue setting ${ChatColor.GOLD}\"ready\" ${ChatColor.RESET}to ${ChatColor.RED}\"false\"${ChatColor.RESET}.")
                            queue.isReady = false

                            true
                        } else {
                            sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}Value must be true or false!")

                            true
                        }
                    }

                    else -> return false
                }
            }

            "announce" -> {
                if (!sender.hasPermission("adminutils.queue.management")) {
                    sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}You don't have permission to use this command!")
                    return true
                }

                if (args.size == 1) {
                    sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}Not enough arguments!")
                    return true
                }

                if (!Main.queues.any { it.id == args[1] }) {
                    sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}The game queue doesn't exist!")
                    return true
                }

                if (Main.queues.first { it.id == args[1] }.isReady) {
                    sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}Unable to announce the game queue, because it is already in a ready state.")
                    return true
                }
                announceGame(args[1])
                sender.sendMessage("${Colors.PREFIX}Successfully announced the game queue.")

                return true
            }

            "transfer" -> {
                if (sender !is Player) {
                    sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}You are not a player!")
                    return true
                }

                if (!sender.hasPermission("adminutils.queue.management")) {
                    sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}You don't have permission to use this command!")
                    return true
                }

                if (args.size == 1 || args.size == 2) {
                    sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}Not enough arguments!")
                    return true
                }

                if (!Main.queues.any { it.id == args[1] }) {
                    sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}The game queue doesn't exist!")
                    return true
                }

                val queue = Main.queues.first { it.id == args[1] }

                if (queue.creator != sender) {
                    sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}You are not the owner of the game queue!")
                    return true
                }

                if (!Main.plugin.server.onlinePlayers.any { it.name == args[2] && it.hasPermission("adminutils.queue.management") }) {
                    sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}Either the player isn't online, or they don't have the ability to manage the queue!")
                    return true
                }

                val newOwner = Main.plugin.server.onlinePlayers.first { it.name == args[2] && it.hasPermission("adminutils.queue.management") }

                queue.creator = newOwner
                sender.sendMessage("${Colors.PREFIX}Successfully transferred game queue ownership to ${ChatColor.GOLD}${newOwner.name}${ChatColor.RESET}!")

                newOwner.sendMessage("${Colors.PREFIX}You have been transferred game queue ownership of the game queue ${ChatColor.AQUA}${queue.name} ${ChatColor.RESET}by ${ChatColor.GOLD}${sender.name}${ChatColor.RESET}!")

                return true
            }

            "teleport", "tp" -> {
                if (sender !is Player) {
                    sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}You are not a player!")
                    return true
                }

                if (!sender.hasPermission("adminutils.queue.management")) {
                    sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}You don't have permission to use this command!")
                    return true
                }

                if (args.size == 1) {
                    sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}Not enough arguments!")
                    return true
                }

                if (!Main.queues.any { it.id == args[1] }) {
                    sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}The game queue doesn't exist!")
                    return true
                }

                val queue = Main.queues.first { it.id == args[1] }

                queue.players.forEach {
                    it.teleport(sender)
                    it.sendMessage("${Colors.PREFIX}You've been teleported to the game queue location.")
                }

                sender.sendMessage("${Colors.PREFIX}Successfully teleported all players.")

                return true
            }

            else -> return false
        }
    }
    
    fun announceGame(id: String) {
        val queue = Main.queues.first { it.id == id }

        val component = TextComponent("\n\n")
        component.addExtra("${ChatColor.GREEN}A game queue has been created for the minigame event ${ChatColor.AQUA}${queue.name}${ChatColor.GREEN} has been created!\n")
        component.addExtra("${ChatColor.GREEN}Would you like to ")

        val clickable = TextComponent("${ChatColor.AQUA}join")
        clickable.hoverEvent = HoverEvent(HoverEvent.Action.SHOW_TEXT, Text("Click me to join the queue!"))
        clickable.clickEvent = ClickEvent(ClickEvent.Action.RUN_COMMAND, "/queue join $id")

        component.addExtra(clickable)
        component.addExtra("${ChatColor.GREEN} the queue?\n\n")

        Main.plugin.server.onlinePlayers.forEach {
            if (queue.players.contains(it)) return@forEach
            it.sendMessage(component)
        }
    }
}