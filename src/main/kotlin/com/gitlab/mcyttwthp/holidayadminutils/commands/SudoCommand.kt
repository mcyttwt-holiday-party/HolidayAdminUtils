package com.gitlab.mcyttwthp.holidayadminutils.commands

import com.gitlab.mcyttwthp.holidayadminutils.Main
import com.gitlab.mcyttwthp.holidayadminutils.util.Colors
import org.bukkit.ChatColor
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.command.TabCompleter
import org.bukkit.entity.Player

object SudoCommand : CommandExecutor, TabCompleter {
    override fun onTabComplete(
        sender: CommandSender,
        command: Command,
        alias: String,
        args: Array<out String>
    ): MutableList<String> {
        if (args.isEmpty()) return mutableListOf("")

        return when (args.size) {
            1 -> {
                Main.plugin.server.onlinePlayers.map { it.name }.toMutableList()
            }

            else -> mutableListOf("")
        }
    }

    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {
        if (args.isEmpty() || args.size == 1) return false

        if (!Main.plugin.server.onlinePlayers.any { it.name == args[0] }) {
            sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}That player does not exist!")
            return true
        }

        try {
            val player = Main.plugin.server.onlinePlayers.filter { it.name == args[0] }[0]
            sender.sendMessage("${Colors.PREFIX}Forcing player ${ChatColor.YELLOW}${player.name} ${ChatColor.RESET}to run command ${ChatColor.GOLD}/${args.slice(1 until args.size).joinToString(" ")}")

            player.performCommand(args.slice(1 until args.size).joinToString(" "))
        } catch (e: Exception) {
            sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}An exception occurred whilst running sudo : ${ChatColor.YELLOW}${e.message}")
            e.printStackTrace()
        }

        return true
    }
}