package com.gitlab.mcyttwthp.holidayadminutils.commands

import com.gitlab.mcyttwthp.holidayadminutils.Main
import com.gitlab.mcyttwthp.holidayadminutils.util.Colors
import org.bukkit.ChatColor
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player

object VanishCommand : CommandExecutor {
    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {
        if (args.size == 1 && !Main.plugin.server.onlinePlayers.any { it.name == args[0] }) {
            sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}That player doesn't exist!")
            return true
        }

        if (args.isEmpty() && sender !is Player) {
            sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}You are not a player!")
            return true
        }

        try {
            val player = if (args.size == 1) Main.plugin.server.onlinePlayers.first { it.name == args[0] } else sender as Player

            if (Main.vanished.contains(player)) {
                Main.vanished.remove(player)

                sender.sendMessage("${Colors.PREFIX}Successfully made player ${ChatColor.YELLOW}${player.name} ${ChatColor.RESET}unvanish!")
                if (player != sender)
                    player.sendMessage("${Colors.PREFIX}You have been taken off vanish by ${ChatColor.YELLOW}${sender.name}${ChatColor.RESET}.")

                Main.plugin.server.onlinePlayers.forEach {
                    it.showPlayer(Main.plugin, player)
                }
            } else {
                Main.vanished.add(player)

                sender.sendMessage("${Colors.PREFIX}Successfully made player ${ChatColor.YELLOW}${player.name} ${ChatColor.RESET}vanish!")

                if (player != sender)
                    player.sendMessage("${Colors.PREFIX}You have been vanished by ${ChatColor.YELLOW}${sender.name}${ChatColor.RESET}!")

                Main.plugin.server.onlinePlayers.forEach {
                    it.hidePlayer(Main.plugin, player)
                }
            }

            return true
        } catch (e: Exception) {
            sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}An exception occurred whilst executing command : ${ChatColor.YELLOW}${e.message}")
            e.printStackTrace()
        }

        return true
    }
}