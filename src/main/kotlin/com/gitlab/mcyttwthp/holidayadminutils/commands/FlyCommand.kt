package com.gitlab.mcyttwthp.holidayadminutils.commands

import com.gitlab.mcyttwthp.holidayadminutils.util.Colors
import org.bukkit.ChatColor
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player

object FlyCommand : CommandExecutor {
    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {
        if (sender !is Player) {
            sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}You are not a player!")
            return true
        }

        sender.isFlying = !sender.isFlying

        sender.sendMessage("${Colors.PREFIX}Toggled your flight!")

        return true
    }

}