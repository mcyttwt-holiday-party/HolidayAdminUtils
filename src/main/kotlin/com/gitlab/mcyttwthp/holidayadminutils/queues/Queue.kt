package com.gitlab.mcyttwthp.holidayadminutils.queues

import org.bukkit.entity.Player

data class Queue(
    val id: String,
    var name: String,
    var maxPlayers: Int,
    val players: MutableList<Player>,
    var creator: Player,
    var isReady: Boolean = false
)