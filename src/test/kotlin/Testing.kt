fun main() {
    val args = listOf("hi", "hello", "man", "hey")

    println(args.slice(2 until args.size).joinToString(" "))
}